<?php

declare(strict_types=1);

namespace PagerWave\Extension\DoctrineOrm\Tests\Fixtures;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="entities")
 */
class Entity
{
    /**
     * @ORM\Column(type="integer")
     */
    public $ranking;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id()
     *
     * @var int
     */
    public $id;

    public function __construct(int $ranking, int $id)
    {
        $this->ranking = $ranking;
        $this->id = $id;
    }
}
