<?php

declare(strict_types=1);

namespace PagerWave\Extension\DoctrineOrm\Tests;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use PagerWave\Definition;
use PagerWave\Extension\DoctrineOrm\QueryBuilderAdapter;
use PagerWave\Extension\DoctrineOrm\Tests\Fixtures\Entity;
use PagerWave\Extension\DoctrineOrm\Tests\Fixtures\EntityDefinition;
use PagerWave\Query;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\Extension\DoctrineOrm\QueryBuilderAdapter
 */
class QueryBuilderAdapterTest extends TestCase
{
    private const TEST_SCHEMA = <<<EOSQL
CREATE TABLE entities (
    id INTEGER NOT NULL,
    ranking INTEGER NOT NULL
)
EOSQL;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @noinspection PhpUnhandledExceptionInspection
     * @noinspection SqlDialectInspection
     * @noinspection SqlNoDataSourceInspection
     */
    public function setUp(): void
    {
        $this->entityManager = EntityManager::create([
            'driver' => 'pdo_sqlite',
            'dsn' => 'sqlite::memory:',
        ], Setup::createAnnotationMetadataConfiguration([
            __DIR__.'/../../Fixtures/Pagination',
        ], true, null, null, false));
        if (method_exists(Connection::class, 'executeStatement')) {
            $this->entityManager->getConnection()->executeStatement(self::TEST_SCHEMA);
        } else {
            /** @noinspection PhpDeprecationInspection */
            $this->entityManager->getConnection()->exec(self::TEST_SCHEMA);
        }
        $this->entityManager->persist(new Entity(3, 8));
        $this->entityManager->persist(new Entity(1, 800));
        $this->entityManager->persist(new Entity(3, 6));
        $this->entityManager->persist(new Entity(10, 12));
        $this->entityManager->persist(new Entity(4, 9));
        $this->entityManager->persist(new Entity(2, 10));
        $this->entityManager->flush();
    }

    public function tearDown(): void
    {
        $this->entityManager->close();
    }

    public function testPaging(): void
    {
        $qb = $this->entityManager->createQueryBuilder()
            ->select('e')
            ->from(Entity::class, 'e');

        $adapter = new QueryBuilderAdapter($qb);
        $results = $adapter->getResults(5, new EntityDefinition(), new Query());

        $this->assertSame([12, 9, 6, 8, 10], array_column($results->getEntries(), 'id'));
        $this->assertSame(800, $results->getNextEntry()->id);
    }

    public function testPagingWithQuery(): void
    {
        $qb = $this->entityManager->createQueryBuilder()
            ->select('e')
            ->from(Entity::class, 'e');

        $adapter = new QueryBuilderAdapter($qb);
        $results = $adapter->getResults(5, new EntityDefinition(), new Query([
            'ranking' => 2,
            'id' => 10,
        ]));

        $this->assertSame([10, 800], array_column($results->getEntries(), 'id'));
        $this->assertNull($results->getNextEntry());
    }

    public function testFieldMapping(): void
    {
        $qb = $this->entityManager->createQueryBuilder()
            ->select('e.id', 'e.ranking AS rank')
            ->from(Entity::class, 'e');

        $adapter = new QueryBuilderAdapter($qb, [], ['rank' => null]);

        $results = $adapter->getResults(
            5,
            new Definition(['rank' => SORT_DESC, 'id' => SORT_ASC]),
            new Query(['rank' => 2, 'id' => 10])
        );

        $this->assertSame([10, 800], array_column($results->getEntries(), 'id'));
    }
}
