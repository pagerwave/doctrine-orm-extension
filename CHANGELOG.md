# v1.2.0 (2023-01-20)

* Allow installation on PHP 8.2.*.

# v1.1.0 (2021-11-30)

* Allow installation on PHP 8.1.*.

* Drop support for PagerWave 1.x.

* Use only PHPUnit 8.

# v1.0.3 (2020-12-04)

* Allow installation on PHP 8.0.*.

* Lower Doctrine ORM version requirement to >= 2.5.

# v1.0.2 (2020-09-27)

* Add dependencies as dev requirements. Version 2 will restore them as non-dev
  requirements.

* Remove PagerWave requirement to avoid circular dependencies.

# v1.0.1 (2020-09-27)

* Remove `doctrine/orm` as a requirement, add as a suggestion instead. Version 2
  will restore it as a requirement.

# v1.0.0 (2020-09-27)

* Split off from main PagerWave repository.
