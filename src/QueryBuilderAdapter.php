<?php

declare(strict_types=1);

namespace PagerWave\Extension\DoctrineOrm;

use Doctrine\ORM\QueryBuilder;
use PagerWave\Adapter\AdapterInterface;
use PagerWave\AdapterResult;
use PagerWave\AdapterResultInterface;
use PagerWave\DefinitionInterface;
use PagerWave\QueryInterface;

/**
 * Paginate a Doctrine ORM `QueryBuilder`.
 *
 * The field names from the definition *MUST NOT* come from user input, or you
 * risk DQL injection!
 */
final class QueryBuilderAdapter implements AdapterInterface
{
    /**
     * @var QueryBuilder
     */
    private $queryBuilder;

    /**
     * @var string[]
     */
    private $typeMap;

    /**
     * @var string[]|null[]|int[]
     */
    private $fieldEntityMap;

    public function __construct(
        QueryBuilder $queryBuilder,
        array $typeMap = [],
        array $fieldEntityMap = []
    ) {
        $this->queryBuilder = $queryBuilder;
        $this->typeMap = $typeMap;
        $this->fieldEntityMap = $fieldEntityMap;
    }

    public function getResults(
        int $max,
        DefinitionInterface $definition,
        QueryInterface $query
    ): AdapterResultInterface
    {
        $qb = clone $this->queryBuilder;
        $qb->setMaxResults($max + 1);

        $alias = $qb->getRootAliases()[0];

        foreach ($definition->getFieldNames() as $field) {
            $desc = $definition->isFieldDescending($field);
            $prefix = \array_key_exists($field, $this->fieldEntityMap)
                ? $this->fieldEntityMap[$field]
                : $alias;
            $expr = $prefix ? "$prefix.$field" : $field;

            if ($query->isFilled()) {
                $type = $this->typeMap[$field] ?? null;
                $elements[] = [$expr, ":next_{$field}", $desc];

                $qb->setParameter("next_$field", $query->get($field), $type);
            }

            $qb->addOrderBy($expr, $desc ? 'DESC' : 'ASC');
        }

        if (isset($elements)) {
            $this->mangleQuery($qb, $elements);
        }

        $results = $qb->getQuery()->execute();
        $pagerEntity = \count($results) > $max ? array_pop($results) : null;

        return new AdapterResult($results, $pagerEntity);
    }

    /**
     * Add a where-clause to the query like:
     *
     * ~~~
     * (a <= 3) AND (a < 3 OR b >= 4) AND (a < 3 AND b > 4 OR c <= 5)
     * ~~~
     *
     * @param array $elements [field, value, is descending]
     */
    private function mangleQuery(QueryBuilder $qb, array $elements): void
    {
        $i = 0;

        $expr = $qb->expr()->andX(...array_map(static function ($field) use ($elements, $qb, &$i) {
            $prev = array_map(static function ($field) use ($qb) {
                return $qb->expr()->{$field[2] ? 'lt' : 'gt'}($field[0], $field[1]);
            }, array_slice($elements, 0, $i++));

            if ($prev) {
                $expr[] = $qb->expr()->andX(...$prev);
            }

            $expr[] = $qb->expr()->{$field[2] ? 'lte' : 'gte'}($field[0], $field[1]);

            return $qb->expr()->orX(...$expr);
        }, $elements));

        $qb->andWhere($expr);
    }
}
