# pagerwave/doctrine-orm-extension

This package lets you paginate [Doctrine ORM][doctrine] query builders using
[PagerWave][pagerwave].

## Installation

~~~
$ composer require pagerwave/doctrine-orm-extension
~~~

## Usage

~~~php
use PagerWave\Extension\DoctrineOrm\QueryBuilderAdapter;

$queryBuilder = $entityManager->createQueryBuilder()
    ->select('p')
    ->from(\App\Entity\Post::class, 'p');

$adapter = new QueryBuilderAdapter($queryBuilder);
~~~

Read the [PagerWave documentation][docs] to learn more.

## Licence

This project is released under the Zlib licence.


[docs]: https://gitlab.com/pagerwave/PagerWave/-/tree/1.x/docs
[doctrine]: https://www.doctrine-project.org/projects/orm.html
[pagerwave]: https://gitlab.com/pagerwave/PagerWave
